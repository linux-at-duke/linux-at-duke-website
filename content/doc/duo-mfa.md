---
date: "1969-12-31T23:59:59-04:00"
description: Brief Howto configure Duo for MFA
categories: 
  - Documentation
keywords:
  - Duo
  - Multifactor Authentication
tags:
  - mfa
  - linux
title: Duo Multifactor Authentication
toc: true
---

## About

Duke University is supporting Duo multifactor authentication for web and host based access.  Duo provides a second factor through a smart phone app, security dongle, physical phone or several other ways.  This page will walk you through getting Duo set up on your Linux server.

## Configuration

Set up your NetID to be multifactor capable: [https://idms-mfa.oit.duke.edu/](https://idms-mfa.oit.duke.edu/)

Install the duo_unix software from: [https://pkg.duosecurity.com/](https://pkg.duosecurity.com/)

Configure Duo to use the Duke account by adjusting your `/etc/duo/login_duo.conf` file to contain the following:

```ini
[duo]
; Duo integration key
ikey = <GET THIS FROM THE DUKE IDMS TEAM>
; Duo secret key
skey = <GET THIS FROM THE DUKE IDMS TEAM>
; Duo API host
host = <GET THIS FROM THE DUKE IDMS TEAM>
; Send command for Duo Push authentication
;pushinfo = yes
; Limit two facter authentication to a subset of users by group
group = <SPACE SEPARATED LIST OF UNIX GROUPS THAT WILL REQUIRE DUO>
```

Add the following line to your /etc/ssh/sshd_config file:

```conf
ForceCommand /usr/sbin/login_duo
```
