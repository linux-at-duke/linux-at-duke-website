---
title: "Kerberos"
date: "1969-12-31T23:59:59-04:00"
tags:
  - linux
  - kerberos
categories: 
 - Documentation
toc: true
---

## Kerberos Client Configuration

### Authentication Configuration

Please note this documentation is out dated and "authconfig" has been replaced by "authselect" in 
Red Hat / CentOS 8 and newer.
Run the following command on your redhat based distribution to enable kerberos/NetID authentication

```bash
/usr/bin/authconfig --kickstart --disablecache --enablemd5 \
--enableldap --ldapserver "ldap.duke.edu" --ldapbasedn \
"ou=People,dc=duke,dc=edu" --enablekrb5 --krb5realm "ACPUB.DUKE.EDU" \
--krb5kdc "kaserv1.acpub.duke.edu:88,kaserv2.acpub.duke.edu:88" \
--krb5adminserver "kaserv1.acpub.duke.edu:749"
```

Sample /etc/krb5.conf file

```ini
######
# KRB for linux connections
######

[logging]
  default = FILE:/var/log/krb5libs.log
  kdc = FILE:/var/log/krb5kdc.log
  admin_server = FILE:/var/log/kadmind.log

[libdefaults]
  ticket_lifetime = 600
  default_realm = ACPUB.DUKE.EDU
  dns_lookup_realm = false
  dns_lookup_kdc = false

[realms]
  ACPUB.DUKE.EDU = {
    kdc = kaserv1.acpub.duke.edu:88
    kdc = kaserv2.acpub.duke.edu:88
    admin_server = kaserv1.acpub.duke.edu:749
    default_domain = duke.edu
  }

[domain_realm]
  .duke.edu = ACPUB.DUKE.EDU
  .oit.duke.edu = ACPUB.DUKE.EDU

[kdc]
  profile = /var/kerberos/krb5kdc/kdc.conf

[appdefaults]
  pam = {
    debug = false
    ticket_lifetime = 36000
    renew_lifetime = 36000
    forwardable = true
  }

##
##

ACPUB.DUKE.EDU = {
                kdc = kaserv1.acpub.duke.edu:88
                kdc = kaserv2.acpub.duke.edu:88
                kdc = kaserv3.acpub.duke.edu:88
                default_domain = duke.edu
                string_to_key_type=mit_string_to_key
        }

[v4_domain_realm]
        .duke.edu = ACPUB.DUKE.EDU
        .oit.duke.edu = ACPUB.DUKE.EDU
        .acpub.duke.edu = ACPUB.DUKE.EDU
```

Sample /etc/ldap.conf file or /etc/openldap/ldap.conf (for user information)

```conf
host ldap.duke.edu

# The distinguished name of the search base.
base dc=duke,dc=edu

ssl no
tls_cacertdir /etc/openldap/cacerts
pam_password md5

# needed to make lookups work for users who have their privacy flag set
nss_map_attribute       gecos   uid

Sample /etc/nsswitch.conf

#
# /etc/nsswitch.conf
#
# An example Name Service Switch config file. This file should be
# sorted with the most-used services at the beginning.
#
# The entry '[NOTFOUND=return]' means that the search for an
# entry should stop if the search in the previous entry turned
# up nothing. Note that if the search failed due to some other reason
# (like no NIS server responding) then the search continues with the
# next entry.
#
# Legal entries are:
#
#   nis or yp       Use NIS (NIS version 2), also called YP
#   dns         Use DNS (Domain Name Service)
#   files           Use the local files
#   db          Use the local database (.db) files
#   compat          Use NIS on compat mode
#   hesiod          Use Hesiod for user lookups
#   ldap            Use LDAP (only if nss_ldap is installed)
#   nisplus or nis+     Use NIS+ (NIS version 3), unsupported
#   [NOTFOUND=return]   Stop searching if not found so far
#

passwd:     files ldap
shadow:     files ldap
group:      files 

hosts:      files dns

bootparams: files
ethers:     files
netmasks:   files
networks:   files
protocols:  files
rpc:        files
services:   files
netgroup:   files
publickey:  files
automount:  files
aliases:    files
```
