---
title: "Public ssh Requirements"
date: 2021-06-11T15:34:48-04:00
author: Jimmy Dorff
tags:
  - "linux"
  - "ssh"
  - "security"
keywords:
  - "ssh"
  - "duo"
  - "authdir"
toc: true
---

Best practices for a Linux system at Duke, and requirements for public ssh access:
 - OS kept updated and running a supported distro
 - Logs are sent to splunk
 - CrowdStrike Falcon is installed
 - MFA via duo for password authenitcation
 - Reporting system information into planisphere

Technical example for these are available here:
[Duke GitLab](https://gitlab.oit.duke.edu/devil-ops/linux-workstation-config)
