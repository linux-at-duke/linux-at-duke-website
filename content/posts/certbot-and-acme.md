---
author: Drew Stinnett
categories:
 - Post
date: "2020-12-15T19:52:00-04:00"
description: ""
keywords:
- TLS
- Certificates
- Magic
- Certbot
- ACME
showFullContent: false
tags:
- linux
- certbot
- acme
title: Certbot and Acme
---

Video recording of the 2020-12-15 meeting on [Certbot](https://certbot.eff.org/) and [ACME](https://en.wikipedia.org/wiki/Automated_Certificate_Management_Environment) is available here: [Duke Box VOD](https://duke.box.com/s/8yxrvmx9vh79ldlufgt87qpwxxtvvyjm)
