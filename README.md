# Linux at Duke Website

## How To Edit The Site

You don't have to have a local copy of hugo but it's kinda fun to so feel free to [install it](https://gohugo.io/getting-started/)

### Without Hugo TLDR

```bash
cp archetypes/default.md content/posts/new-post.md
vim content/posts/new-post.md # replace the stuff in the top bit (frontmatter)
# Be sure to remove `draft: true` when you're ready to publish!
git add content/posts/new-post.md; git commit content/posts/new-post.md && git push
```

### WITH Hugo TLDR

```bash
hugo new posts/new-post.md
vim content/posts/new-post.md # you won't have as much to edit as hugo fills in details
# Be sure to remove `draft: true` when you're ready to publish!
git add content/posts/new-post.md; git commit content/posts/new-post.md && git push
```

### Tell Nate to make this page better
